package com.greatlearning.implementation;
import com.greatlearning.interfaces.AllMovies;

public class AllMoviesFactory {
	public static AllMovies createAllMovies(String channel) {
		if (channel == null || channel.isEmpty())
			return null;
		if ("moviescoming".equals(channel)) {
			return new MoviesComing();
		} else if ("moviesinTheater".equals(channel)) {
			return new MoviesInTheatres();
		} else if ("TopRatedIndia".equals(channel)) {
			return new TopRatedIndia();
		} else if ("topRatedMovies".equals(channel)) {
			return new TopRatedMovies();
		}

		return null;

	}
}
