package com.greatlearning.implementation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.Movie;
import com.greatlearning.interfaces.AllMovies;
import com.greatlearning.resource.DbResource;
public class TopRatedIndia implements AllMovies {
	public List<Movie> getMovies() throws SQLException 
	{
		try {
			Connection con = DbResource.getDbConnection();
		PreparedStatement ps = con.prepareStatement("select *from topratingindian");
		ResultSet rs = ps.executeQuery();
		List<Movie> ls = new ArrayList<>();

		while (rs.next()) {
			Movie emp = new Movie();
			emp.setId(rs.getInt("id"));
			emp.setName(rs.getString("name"));
			emp.setType(rs.getString("type"));
			emp.setYear(rs.getInt("year"));
			emp.setRating(rs.getInt("rating"));
			ls.add(emp);
		}
		return ls;
	}
		catch(Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public int add(Movie movie) throws SQLException
	{
		try {
			Connection con = DbResource.getDbConnection();
		PreparedStatement ps = con.prepareStatement("insert into topratingindian values(?,?,?,?,?)");

		ps.setInt(1, movie.getId());
		ps.setString(2, movie.getName());
		ps.setString(3, movie.getType());
		ps.setInt(4, movie.getYear());
		ps.setInt(5,movie.getRating());
		return ps.executeUpdate();
	}catch(Exception e) {
		System.out.println(e);
		return 0;
	}
	}
	 
	public int update(Movie movie) throws SQLException
	{
		try {
			Connection con = DbResource.getDbConnection();
		PreparedStatement ps = con.prepareStatement("update topratingindian set id=? where name=?");
		
		ps.setInt(1, movie.getId());
		ps.setString(2, movie.getName());
		return ps.executeUpdate();
		}catch(Exception e) {
			System.out.println(e);
			return 0;
		}
		
	}
	
	public int delete(Movie movie) throws SQLException
	
	{
		try {
			Connection con = DbResource.getDbConnection();
		PreparedStatement ps = con.prepareStatement("delete from topratingindian where name=?");
		ps.setString(2, movie.getName());
		return ps.executeUpdate();
	}catch(Exception e) {
		System.out.println(e);
		return 0;
	}
}
}

