package com.greatlearning.test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.sql.SQLException;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.greatlearning.implementation.TopRatedMovies;
import com.greatlearning.bean.Movie;

class TopRatedMoviesTest {

	//@Test
	void testGetMovie() throws SQLException {
		//fail("Not yet implemented");
		TopRatedMovies mc=new TopRatedMovies();
		List<Movie> lo= mc.getMovies();
		
		assertEquals(10, lo.size());
	}

	//@Test
	void testAdd() throws SQLException {
		//fail("Not yet implemented");
		TopRatedMovies mc=new TopRatedMovies();
		Movie m=new Movie();
		m.setId(1);
		m.setName("pournami");
		m.setYear(2013);
		m.setRating(4);
		int success=mc.add(m);
		assertEquals(1,success);
	}

	@Test
	void testUpdate() throws SQLException {
		//fail("Not yet implemented");
		TopRatedMovies mc=new TopRatedMovies();
		Movie m=new Movie();
		m.setId(1);
		m.setName("gangleader");
		mc.update(m);
		assertTrue(m.getName()=="gangleader");
	}

	@Test
	void testDelete() throws SQLException {
		//fail("Not yet implemented");
		TopRatedMovies mc=new TopRatedMovies();
		Movie m=new Movie();
		m.setName("mr.perfect");
		mc.delete(m);
		assertTrue(m.getName()=="mr.perfect");
	}

}

