package com.greatlearning.test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.sql.SQLException;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.greatlearning.implementation.TopRatedIndia;
import com.greatlearning.bean.Movie;

class TopRatedIndiaTest {

	//@Test
	void testGetMovie() throws SQLException {
		//fail("Not yet implemented");
		TopRatedIndia mc=new TopRatedIndia();
		List<Movie> lo= mc.getMovies();
		
		assertEquals(10, lo.size());
	}

	//@Test
	void testAdd() throws SQLException {
		//fail("Not yet implemented");
		TopRatedIndia mc=new TopRatedIndia();
		Movie m=new Movie();
		m.setId(1);
		m.setName("rebel");
		m.setYear(2010);
		m.setRating(3);
		int success=mc.add(m);
		assertEquals(1,success);
	}

	@Test
	void testUpdate() throws SQLException {
		//fail("Not yet implemented");
		TopRatedIndia mc=new TopRatedIndia();
		Movie m=new Movie();
		m.setId(1);
		m.setName("eega");
		mc.update(m);
		assertTrue(m.getName()=="eega");
	}

	@Test
	void testDelete() throws SQLException {
		//fail("Not yet implemented");
		TopRatedIndia mc=new TopRatedIndia();
		Movie m=new Movie();
		m.setName("pushpa");
		mc.delete(m);
		assertTrue(m.getName()=="pushpa");
	}


}

