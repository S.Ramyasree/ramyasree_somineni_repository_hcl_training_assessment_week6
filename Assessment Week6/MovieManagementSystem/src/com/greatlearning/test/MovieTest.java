package com.greatlearning.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.greatlearning.bean.Movie;

class MovieTest {
	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		Movie m1=new Movie();
		m1.setId(1);
		assertTrue(m1.getId()==1);
	}

	@Test
	public void testSetId() {
		//fail("Not yet implemented");
		Movie m1=new Movie();
		m1.setId(1);
		assertTrue(m1.getId()==1);
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		Movie m1=new Movie();
		m1.setName("avengers");
		assertTrue(m1.getName()=="avengers");
	}

	@Test
	public void testSetName() {
		//fail("Not yet implemented");
		Movie m1=new Movie();
		m1.setName("avathar");
		assertTrue(m1.getName()=="avathar");
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		Movie m1=new Movie();
		m1.setYear(2010);
		assertTrue(m1.getYear()==2010);
	}

	@Test
	public void testSetYear() {
		//fail("Not yet implemented");
		Movie m1=new Movie();
		m1.setYear(2010);
		assertTrue(m1.getYear()==2010);
		
	}

}
