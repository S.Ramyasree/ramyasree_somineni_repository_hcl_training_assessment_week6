package com.greatlearning.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.greatlearning.implementation.MoviesComing;
import com.greatlearning.bean.Movie;

class MoviesComingTest {
	//@Test
	void testGetMovie() throws SQLException {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		List<Movie> lo= mc.getMovies();
		
		assertEquals(10, lo.size());
	}

	//@Test
	void testAdd() throws SQLException {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		Movie m=new Movie();
		m.setId(1);
		m.setName("bunny");
		m.setYear(2017);
		m.setRating(2);
		int success=mc.add(m);
		assertEquals(1,success);
	}

	@Test
	void testUpdate() throws SQLException {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		Movie m=new Movie();
		m.setId(1);
		m.setName("akhil");
		mc.update(m);
		assertTrue(m.getName()=="akhil");
	}

	@Test
	void testDelete() throws SQLException {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		Movie m=new Movie();
		m.setName("maharshi");
		mc.delete(m);
		assertTrue(m.getName()=="maharshi");
	}

}


