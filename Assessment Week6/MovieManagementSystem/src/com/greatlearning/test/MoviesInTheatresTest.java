package com.greatlearning.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.greatlearning.implementation.MoviesInTheatres;
import com.greatlearning.bean.Movie;

class MoviesInTheatresTest {

	//@Test
	void testGetMovie() throws SQLException {
		//fail("Not yet implemented");
		MoviesInTheatres mc=new MoviesInTheatres();
		List<Movie> lo= mc.getMovies();
		
		assertEquals(10, lo.size());
	}

	//@Test
	void testAdd() throws SQLException {
		//fail("Not yet implemented");
		MoviesInTheatres mc=new MoviesInTheatres();
		Movie m=new Movie();
		m.setId(1);
		m.setName("avunu");
		m.setYear(2020);
		m.setRating(3);
		int success=mc.add(m);
		assertEquals(1,success);
	}

	@Test
	void testUpdate() throws SQLException {
		//fail("Not yet implemented");
		MoviesInTheatres mc=new MoviesInTheatres();
		Movie m=new Movie();
		m.setId(1);
		m.setName("arjun");
		mc.update(m);
		assertTrue(m.getName()=="arjun");
	}

	@Test
	void testDelete() throws SQLException {
		//fail("Not yet implemented");
		MoviesInTheatres mc=new MoviesInTheatres();
		Movie m=new Movie();
		m.setName("sahoo");
		mc.delete(m);
		assertTrue(m.getName()=="sahoo");
	}


}

