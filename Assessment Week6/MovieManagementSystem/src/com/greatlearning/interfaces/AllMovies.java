package com.greatlearning.interfaces;
import java.sql.SQLException;
import java.util.List;

import com.greatlearning.bean.Movie;
public interface AllMovies {
	public List<Movie> getMovies() throws SQLException;

	public int add(Movie movie) throws SQLException;
	 
	public int update(Movie update) throws SQLException;
	
	public int delete(Movie delete) throws SQLException;

}
